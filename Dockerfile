FROM sysrun/rtlsdr-base:0.1

MAINTAINER Frederik Granna

WORKDIR /tmp

ENV commit_id 8bcc068d74f86968bd24e7c7c8c62364086d9feb

RUN git clone https://github.com/merbanan/rtl_433.git && \
    cd rtl_433 && \
    git reset --hard $commit_id && \
    mkdir build && \
    cd build && \
    cmake ../ && \
    make && \
    make install && \
    cd / && \
    rm -rf /tmp/rtl_433

WORKDIR /

ENTRYPOINT ["rtl_433"]
